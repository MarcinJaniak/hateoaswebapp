﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Transactions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using RiskFirst.Hateoas;
using RiskFirst.Hateoas.Models;

namespace HateOasWebApp.Controllers
{
    [Route("api/[controller]")]
    public class ValuesController : Controller
    {
        private IEnumerable<Person> _personCollection;
        private readonly ILinksService _linksService;

        public ValuesController(ILinksService linksService)
        {
            _linksService = linksService;
            _personCollection = new[]
            {
                new Person() {FirstName = "Marcin", LastName = "Janiak", id = 1, Age = 24},
                new Person() {FirstName = "Adam", LastName = "Nowak", id = 2, Age = 55},
                new Person() {FirstName = "Jan", LastName = "Kowalski", id = 3, Age = 55}
            };
        }

        // GET api/values
        [HttpGet(Name = "GetAllPeople")]
        public async Task<IActionResult> Get()
        {
            var result = _personCollection.ToList();

            foreach (var person in result)
            {
                await _linksService.AddLinksAsync(person);
            }
            var itemsResult = new ItemsLinkContainer<Person>()
            {
                Items = result
            };
            await _linksService.AddLinksAsync(itemsResult);
            return Ok(itemsResult);
        }

        // GET api/values/5
        [HttpGet("{id}", Name = "GetPersonById")]
        public async Task<IActionResult> Get(int id)
        {
            var singlePerson = _personCollection.FirstOrDefault(x => x.id == id);
            await _linksService.AddLinksAsync(singlePerson, "DetailsPolicy");
            return singlePerson != null ? (IActionResult) Ok(singlePerson) : NotFound();
        }

        // POST api/values
        [HttpPost(Name = "InsertPerson")]
        public IActionResult Post([FromBody] Person value)
        {
            _personCollection.ToList().Add(value);

            return Ok();
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }


        [HttpDelete("{id}", Name = "DeleteSinglePerson")]
        public IActionResult Delete(int id)
        {
            _personCollection = _personCollection.Where(x => x.id != id).ToList();

            return Ok();
        }
    }
}