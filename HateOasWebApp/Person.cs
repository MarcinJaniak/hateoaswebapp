﻿using RiskFirst.Hateoas.Models;

namespace HateOasWebApp
{
    public class Person:LinkContainer 
    {
        public int id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int Age { get; set; }
    }
}