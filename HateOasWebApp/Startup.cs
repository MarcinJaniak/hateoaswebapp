﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using RiskFirst.Hateoas;
using RiskFirst.Hateoas.Models;

namespace HateOasWebApp
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc();

            services.AddLinks(config =>
            {
                config.AddPolicy<Person>(policy =>
                {
                    policy.RequireRoutedLink("self", "GetPersonById", x => new { id = x.id });
                });
                
                config.AddPolicy<ItemsLinkContainer<Person>>(policy =>
                {
                    policy.RequireSelfLink()
                        .RequireRoutedLink("insert", "InsertPerson");
                });
                
                config.AddPolicy<Person>("DetailsPolicy",policy =>
                {
                    policy.RequireSelfLink()
                    .RequireRoutedLink("delete", "DeleteSinglePerson", x => new {id = x.id});
                });
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseMvc();
        }
    }
}